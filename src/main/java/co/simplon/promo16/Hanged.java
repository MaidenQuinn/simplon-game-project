package co.simplon.promo16;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Hanged {

    Scanner input = new Scanner(System.in);
    char guessLetter;
    static List<String> wordToFindListFromLib = new ArrayList<>();
    static String wordToFind;
    String hiddenWord;
    int wrongAnswer = 0;
    Scanner dictionnaryScanner ;

    public void playHangedGame() {
        initializeGame();
        startGame();
    }

    /**
     * Initialisation du mot à trouver et du compteur de vies, ainsi que des règles
     * du jeu.
     */
    private void initializeGame() {
        try {
            dictionnaryScanner = new Scanner(new File("/home/kevin/Bureau/Simplon/Projets/game/dico.txt"));
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (dictionnaryScanner.hasNextLine()) {
            wordToFindListFromLib.add(dictionnaryScanner.nextLine());
        }
        Random rand = new Random();
        int randomIndex = rand.nextInt(wordToFindListFromLib.size());

        wrongAnswer = 0;
        wordToFind = wordToFindListFromLib.get(randomIndex).toUpperCase();
        hiddenWord = wordToFind.replaceAll("[A-Z]", "_ ");

        // System.out.println("Mode développeur (TRICHE) - Le mot à trouver est : " +
        // wordToFind);
        System.out.println(
                """
                                 _
                                | |
                                | |__   __ _ _ __   __ _ _ __ ___   __ _ _ __
                                | '_ \\ / _` | '_ \\ / _` | '_ ` _ \\ / _` | '_ \\
                                | | | | (_| | | | | (_| | | | | | | (_| | | | |
                                |_| |_|\\__,_|_| |_|\\__, |_| |_| |_|\\__,_|_| |_|
                                                    __/ |
                                                   |___/
                        Bienvenue dans le jeu du Pendu !
                        Vous disposez de 5 vies.
                        L'objectif est de retrouver le mot caché en proposant une série de lettre.
                        Attention ! Chaque mauvaise réponse ou réponse vide vous enlèvera 1 vie.
                        Vous pouvez arrêter le jeu à tous moment en tapant 'Exit' ou 'Quit'.
                        Bon jeu à vous !
                                """);
    }

    /**
     * Lancement du jeu
     */
    private void startGame() {

        boolean guessesContainsGuess;
        String guesses = "";

        while (wrongAnswer < 5 && hiddenWord.contains("_")) {
            System.out.println(hiddenWord + "\n");
            int livesLeft = 5 - wrongAnswer;
            if (livesLeft > 0) {
                System.out.println("Il vous reste " + livesLeft + " tentative(s)\n");
            }
            System.out.println("Votre lettre :");

            // Doit être passé en uppercase pour faire la vérification avec le mot caché
            String nextLine = input.nextLine();

            if (nextLine.length() > 0) {
                guessLetter = nextLine.toUpperCase().charAt(0);
            }

            guessesContainsGuess = (guesses.indexOf(guessLetter)) != -1;

            guesses += guessLetter;
            stopGame(nextLine);
            checkIfLetterIsAlreadyFound(guessesContainsGuess);
            checkIfLetterIsInWord(guessLetter);
        }
        if(wrongAnswer < 5) {
            checkIfUserHaveWin(wrongAnswer);
        }

    }

    /**
     * Met fin au jeu si le joueur tape un des mots
     * clé nécessaire à l'arrêt de l'application.
     * 
     * @param word
     */
    private void stopGame(String word) {
        if (word.toLowerCase().equals("quit") || word.toLowerCase().equals("quitter")
                || word.toLowerCase().equals("exit")) {
            System.exit(0);
        }
    }

    /**
     * Vérifie si la lettre en paramètre est présente ou non
     * dans le mot caché.
     * 
     * @param guessLetter
     */
    private void checkIfLetterIsInWord(char guessLetter) {
        boolean guessInWord = (wordToFind.indexOf(guessLetter)) != -1;
        if (guessInWord) {
            System.out.println("La lettre " + guessLetter + " est présente dans le mot. \n");

            replaceHiddenWordWithChar();
        }

        else {
            System.out.println("La lettre " + guessLetter + " n'est pas présente dans le mot.");
            wrongAnswer++;
            System.out.println(printHangedMan(wrongAnswer));
        }
    }

    private void checkIfLetterIsAlreadyFound(boolean guessesContainsGuess) {

        if (guessesContainsGuess) {
            System.out.println("Vous avez déjà trouvé la lettre "
                    + guessLetter + ". \n");
        }
    }

    /**
     * Remplace les underscores du mot caché par les lettre qui ont été trouvées
     * si elle sont présentes dans le mot
     */
    private void replaceHiddenWordWithChar() {
        for (int position = 0; position < wordToFind.length(); position++) {
            if (wordToFind.charAt(position) == guessLetter
                    && hiddenWord.charAt(position) != guessLetter) {

                hiddenWord = hiddenWord.replaceAll("_ ", "_");
                String tempWord = hiddenWord.substring(0, position)
                        + guessLetter
                        + hiddenWord.substring(position + 1);
                tempWord = tempWord.replaceAll("_", "_ ");
                hiddenWord = tempWord;
            }
        }
    }

    /**
     * Vérifie si le joueur à gagné ou perdu
     */
    private void checkIfUserHaveWin(int wrongAnswer) {
        System.out.println("Le mot caché est: " + wordToFind + "\nBien joué, vous avez réussi !");
        newGame();
    }

    /**
     * Relance une partie à la fin de la première si l'utilisateur accepte.
     */
    private void newGame() {
        System.out.println("Souhaitez-vous recommencer une nouvelle partie ? [y/N]");
        String newGame = input.nextLine().toLowerCase();
        if (newGame.equals("y") || newGame.equals("yes")) {
            System.out.println(newGame);
            playHangedGame();
        }
    }

    /**
     * 
     * @param wrongAnswer
     */
    private String printHangedMan(int wrongAnswer) {
        switch (wrongAnswer) {
            case 1:
                return """





                         _____________
                        """;
            case 2:
                return """

                            |
                            |
                            |
                            |
                            |
                            |
                         ___|_________
                        """;
            case 3:
                return """
                             _______
                            |/
                            |
                            |
                            |
                            |
                            |
                         ___|_________
                        """;
            case 4:
                return """
                             _______
                            |/      |
                            |       |
                            |
                            |
                            |
                            |
                         ___|_________
                        """;
            case 5:
                return """
                            _______
                           |/      |
                           |       |
                           |     (x_x)
                           |      /|\\
                           |       |
                           |      / \\
                        ___|_________

                        Vous avez PERDU ! Vous n'avez plus de vie.
                        """
                        + "Le mot attendu était: " + wordToFind + ". Dommage !";
            default:
                return "";
        }
    }
}
