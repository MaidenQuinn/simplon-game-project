# HANGMAN
```
 _                                             
| |                                            
| |__   __ _ _ __   __ _ _ __ ___   __ _ _ __  
| '_ \ / _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
| | | | (_| | | | | (_| | | | | | | (_| | | | |
|_| |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                    __/ |                      
                   |___/                       
```
# Principe

Création d'un jeu du pendu sur le terminal jouable en intéragissant avec la console de commande.
L'objectif est de retrouver un mot caché en proposant une série de lettres.
Le jeu est développé en Java 17.

